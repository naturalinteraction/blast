# Blast

## Class Diagram

![diagram](class_diagram.jpg "Blast Class Diagram")

## Demo Video

[Video](https://photos.app.goo.gl/HdLiHNDmdFKbkHEdA)

## Installation

On Ubuntu 16.04:

* download and install OpenFrameworks for Linux 64-bit gcc 5 `https://openframeworks.cc/download` (version `0.11.0-master`)
* run the `install_dependencies.sh` and `install_codecs.sh` scripts

## Development

* check out the run script `r.sh` and the debugger script `d.sh`
