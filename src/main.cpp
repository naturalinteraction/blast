
// © 2020 Natural Interaction


#include "ofMain.h"
#include "blastApp.h"


int main()
{
    ofSetupOpenGL(890, 920, OF_WINDOW);
    ofRunApp( new blastApp() );
}
