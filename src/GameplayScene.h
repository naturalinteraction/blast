
// © 2020 Natural Interaction


#ifndef GameplayScene_h
#define GameplayScene_h

#include "ofMain.h"
#include "globals.h"
#include "Block.h"
#include "Scene.h"


class GameplayScene  :  public Scene
{

  public:

    GameplayScene();
    ~GameplayScene();

    void update();
    void draw();
    void drawBoardBackground();
    void drawCountdownAndScore();

    void replaceEmptyBlocks();

    void invalidatePressed();
    void tap(int row, int column);

    int floodFillAndCountAndPossiblyDestroy(int r, int c, BlockType previous_type, BlockType new_type, bool destroy);

    void mouseMoved(ofMouseEventArgs & args);
    void mouseDragged(ofMouseEventArgs & args);
    void mousePressed(ofMouseEventArgs & args);
    void mouseReleased(ofMouseEventArgs & args);
    void mouseScrolled(ofMouseEventArgs & args);
    void mouseEntered(ofMouseEventArgs & args);
    void mouseExited(ofMouseEventArgs & args);

    bool isBoardSteady();
    bool isBoardFull();
    bool isBoardCompletelyEmpty();
    bool isBoardFinished();

    void incrementScore(int blocks);
    void incrementTimeout(int blocks);

    vector< vector <Block> > board;

    float countdown_timeout;

    float pressed_row;
    float pressed_column;

};

#endif
