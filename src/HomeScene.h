
// © 2020 Natural Interaction


#ifndef HomeScene_h
#define HomeScene_h

#include "ofMain.h"
#include "globals.h"
#include "Button.h"
#include "Scene.h"


class HomeScene  :  public Scene
{

  public:

    HomeScene();
    ~HomeScene();

    void update();
    void draw();
    void drawScore();

    void mouseMoved(ofMouseEventArgs & args);
    void mouseDragged(ofMouseEventArgs & args);
    void mousePressed(ofMouseEventArgs & args);
    void mouseReleased(ofMouseEventArgs & args);
    void mouseScrolled(ofMouseEventArgs & args);
    void mouseEntered(ofMouseEventArgs & args);
    void mouseExited(ofMouseEventArgs & args);

    Button start_new_game_button;

};

#endif
