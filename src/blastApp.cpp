
// © 2020 Natural Interaction


#include "blastApp.h"

int highest_score;  // declared in globals.h
int score;          // declared in globals.h


void blastApp::setup()
{
    ofSetLogLevel(OF_LOG_VERBOSE);
    ofSetFrameRate(60);
    ofSetVerticalSync(true);
    ofSetCircleResolution(32);

    ofSetWindowTitle("Blast");

    ofBackground(BACKGROUND_BRIGHTNESS);

    current_scene = new HomeScene();

    highest_score = 123;  // would be read from disk
}

void blastApp::update()
{
    if (current_scene)  current_scene -> update();
}

void blastApp::draw()
{
    if (current_scene)  current_scene -> draw();
}

void blastApp::gotMessage(ofMessage msg)
{
    if (current_scene)  delete current_scene;

    if (msg.message == "gameplay")
      current_scene = new GameplayScene();
    else if (msg.message == "gameover")
      current_scene = new GameoverScene();
    else
      current_scene = new HomeScene();
}

void blastApp::keyPressed(int key)  {}
void blastApp::keyReleased(int key)  {}
void blastApp::mouseMoved(int x, int y )  {}
void blastApp::mouseDragged(int x, int y, int button)  {}
void blastApp::mousePressed(int x, int y, int button)  {}
void blastApp::mouseReleased(int x, int y, int button)  {}
void blastApp::mouseEntered(int x, int y)  {}
void blastApp::mouseExited(int x, int y)  {}
void blastApp::windowResized(int w, int h)  {}
void blastApp::dragEvent(ofDragInfo dragInfo)  {}
