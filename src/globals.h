
// © 2020 Natural Interaction


#ifndef globals_h
#define globals_h

#define BACKGROUND_BRIGHTNESS   64

#define PADDING              8
#define BOARD_BRIGHTNESS    96
#define BOARD_SIZE           8  // 8x8 board
#define BLOCK_SCALE_SPEED  5.0
#define TIMEOUT_SECONDS 60 * 2  // 2 minutes
#define BLOCK_SIZE         100
#define INVALID             -1

extern int highest_score;
extern int score;

#endif