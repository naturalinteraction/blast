
// © 2020 Natural Interaction


#ifndef Scene_h
#define Scene_h

#include "ofMain.h"


// we need this superclass just so we can write more compact code in blastApp
class Scene
{

  public:

    Scene();
    virtual ~Scene();

    virtual void update();
    virtual void draw();
};

#endif
