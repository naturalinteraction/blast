
// © 2020 Natural Interaction


#include "GameoverScene.h"


// the gameover scene with the player score, the highest score, a button to start a new game and a button to go to the home scene
GameoverScene::GameoverScene()
{
    ofRegisterMouseEvents(this);

    start_new_game_button.setup("Start New Game", ofRectangle(2 * PADDING,
                                                              ofGetHeight() * 0.5,
                                                              320,                  // width
                                                              32),                  // height
                                "gameplay");  // message

    go_to_home_button.setup    ("Go To Home",     ofRectangle(2 * PADDING,
                                                              ofGetHeight() * 0.5 + 40,
                                                              320,                  // width
                                                              32),                  // height
                                "home");  // message
}

GameoverScene::~GameoverScene()
{
    ofUnregisterMouseEvents(this);
}

void GameoverScene::update()
{
    start_new_game_button.update();
    go_to_home_button.update();
}

void GameoverScene::drawScores()
{
    ofSetColor(255);

    int integer_score = floor(highest_score);
    ofDrawBitmapString("Highest Score    " + to_string(integer_score),
                       PADDING * 3,
                       ofGetHeight() * 0.5 - PADDING * 4);

    integer_score = floor(score);
    ofDrawBitmapString("Your Score       " + to_string(integer_score),
                       PADDING * 3,
                       ofGetHeight() * 0.5 - PADDING * 6);

    ofDrawBitmapString("Game Over",
                       PADDING * 3,
                       ofGetHeight() * 0.5 - PADDING * 10);
}

void GameoverScene::draw()
{
    ofFill();

    drawScores();

    start_new_game_button.draw();
    go_to_home_button.draw();
}

void GameoverScene::mousePressed(ofMouseEventArgs & args)
{
    // could be verified in mouseReleased()
    if (start_new_game_button.inside(args.x, args.y))
        start_new_game_button.state = ButtonVanishing;

    if (go_to_home_button.inside(args.x, args.y))
        go_to_home_button.state = ButtonVanishing;
}

void GameoverScene::mouseMoved(ofMouseEventArgs & args)  {}
void GameoverScene::mouseDragged(ofMouseEventArgs & args)  {}
void GameoverScene::mouseReleased(ofMouseEventArgs & args)  {}
void GameoverScene::mouseScrolled(ofMouseEventArgs & args)  {}
void GameoverScene::mouseEntered(ofMouseEventArgs & args)  {}
void GameoverScene::mouseExited(ofMouseEventArgs & args)  {}
