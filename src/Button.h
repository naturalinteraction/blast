
// © 2020 Natural Interaction


#ifndef Button_h
#define Button_h

#include "ofMain.h"
#include "globals.h"

enum ButtonState { ButtonSteady, ButtonAppearing, ButtonVanishing };


class Button
{

  public:

    Button();
    ~Button();

    void setup(const string &text, const ofRectangle &rectangle, const string &command);
    void update();
    void draw();

    bool inside(float x, float y);

    string text;
    ofRectangle rectangle;  // where is it
    bool flash;
    float scale;
    bool tapped;
    ButtonState state;
    string command;
};

#endif
