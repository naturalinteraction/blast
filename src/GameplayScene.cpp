
// © 2020 Natural Interaction


#include "GameplayScene.h"


// the gameplay scene with the game board, the remaining time and the current score
GameplayScene::GameplayScene()
{
    ofRegisterMouseEvents(this);

    board.resize(BOARD_SIZE);
    for (int i = 0; i < BOARD_SIZE; i++)
      board[i].resize(BOARD_SIZE);

    for (int r = 0; r < BOARD_SIZE; r++)
      for (int c = 0; c < BOARD_SIZE; c++)
        board[r][c].setup(r, c);

    countdown_timeout = ofGetElapsedTimef() + TIMEOUT_SECONDS;
    score = 0;

    replaceEmptyBlocks();
}

void GameplayScene::incrementScore(int blocks)
{
    if (! blocks)
      return;

    score += (blocks - 1) * 80 + pow(((blocks - 2) / 5.0), 2);
}

void GameplayScene::incrementTimeout(int blocks)
{
    if (! blocks)
      return;

    countdown_timeout += 10 + 20 * pow(((blocks - 2) / 3.0), 2);
}

GameplayScene::~GameplayScene()
{
    ofUnregisterMouseEvents(this);
}

void GameplayScene::replaceEmptyBlocks()
{
    for (int r = 0; r < BOARD_SIZE; r++)
      for (int c = 0; c < BOARD_SIZE; c++)
        if (board[r][c].type == Empty)
        {
          if (ofRandom(100) <= 5)  // a special block (5% probability) with no color definition
          {
              board[r][c].type = (ofRandom(100) < 50)  ?  Stripe : Bomb;
          }
          else  // a color block (95% probability)
          {
              board[r][c].type = (BlockType)ofRandom(Red, Orange + 1);
          }

          board[r][c].state = Appearing;
        }
}

void GameplayScene::update()
{
      for (int r = 0; r < BOARD_SIZE; r++)
        for (int c = 0; c < BOARD_SIZE; c++)
          board[r][c].update();

      if (isBoardFinished() || countdown_timeout <= ofGetElapsedTimef())
      {
          for (int c = 0; c < BOARD_SIZE; c++)
              for (int r = 0; r < BOARD_SIZE; r++)
              {
                board[r][c].state = Vanishing;
              }
          if (score > highest_score)
            highest_score = score;  // and save it to disk
      }

      // When a valid move is detected the matching blocks are destroyed and replaced by other randomly generated blocks.
      if (isBoardSteady() && ! isBoardFull())
        replaceEmptyBlocks();

      if (isBoardCompletelyEmpty())
        ofSendMessage("gameover");
}

void GameplayScene::drawBoardBackground()
{
    ofSetColor(BOARD_BRIGHTNESS);

    float side = BOARD_SIZE * BLOCK_SIZE + (BOARD_SIZE + 1) * PADDING;

    ofDrawRectRounded(PADDING, PADDING,  // x,y
                      side, side,        // width,height
                      PADDING);          // corner radius
}

void GameplayScene::drawCountdownAndScore()
{
    ofSetColor(255);

    int seconds = countdown_timeout - ofGetElapsedTimef();

    int minutes = seconds / 60;
    seconds = seconds % 60;

    string leading_zeros = "";
    if (seconds < 10)
      leading_zeros = "0";

    ofDrawBitmapString("Time Left  " + to_string(minutes) + ":" + leading_zeros + to_string(seconds),
                       2 * PADDING,
                       4 * PADDING + (PADDING + BLOCK_SIZE) * BOARD_SIZE);

    int integer_score = floor(score);

    ofDrawBitmapString("Score      " + to_string(integer_score),
                       2 * PADDING,
                       4 * PADDING + (PADDING + BLOCK_SIZE) * BOARD_SIZE + 15);
}

void GameplayScene::draw()
{
    ofFill();

    drawBoardBackground();

    for (int r = 0; r < BOARD_SIZE; r++)
      for (int c = 0; c < BOARD_SIZE; c++)
          board[r][c].draw();

    drawCountdownAndScore();
}

void GameplayScene::mousePressed(ofMouseEventArgs & args)
{
    for (int r = 0; r < BOARD_SIZE; r++)
      for (int c = 0; c < BOARD_SIZE; c++)
          if (board[r][c].insideAndTappable(args.x, args.y))
          {
            pressed_row = r;
            pressed_column = c;
            return;
          }

    invalidatePressed();
}

void GameplayScene::invalidatePressed()
{
    pressed_row    = INVALID;
    pressed_column = INVALID;
}

// recursive, could be done in a while loop
int GameplayScene::floodFillAndCountAndPossiblyDestroy(int r, int c, BlockType previous_type, BlockType new_type, bool destroy)
{
    if (board[r][c].type != previous_type)
        return 0;

    int count = 0;

    board[r][c].type = new_type;
    if (destroy)
      board[r][c].state = Vanishing;

    // Consecutive blocks in the diagonal direction are not allowed.
    if (r < BOARD_SIZE - 1) count += floodFillAndCountAndPossiblyDestroy(r + 1, c, previous_type, new_type, destroy);
    if (r > 0)              count += floodFillAndCountAndPossiblyDestroy(r - 1, c, previous_type, new_type, destroy);
    if (c < BOARD_SIZE - 1) count += floodFillAndCountAndPossiblyDestroy(r, c + 1, previous_type, new_type, destroy);
    if (c > 0)              count += floodFillAndCountAndPossiblyDestroy(r, c - 1, previous_type, new_type, destroy);

    return ++count;
}

void GameplayScene::tap(int row, int column)
{
    int destroyed_blocks = 0;

    if (board[row][column].type == Stripe)
    {
        // The stripe identifies all the blocks in the row where the stripe is, including the stripe block tapped by the user.
        for (int c = 0; c < BOARD_SIZE; c++)
        {
            board[row][c].state = Vanishing;
            destroyed_blocks++;
        }
    }
    else if (board[row][column].type == Bomb)
    {
        // The bomb identifies the eight (or less if the bomb is on the edge) blocks surrounding the bomb independently of the color, plus the bomb itself.
        for (int c = max(0, column - 1); c < min(BOARD_SIZE, column + 2); c++)
          for (int r = max(0, row - 1); r < min(BOARD_SIZE, row + 2); r++)
          {
            board[r][c].state = Vanishing;
            destroyed_blocks++;
          }
    }
    else
    {
        BlockType tapped_type = board[row][column].type;

        int consecutive_count = floodFillAndCountAndPossiblyDestroy(row, column, tapped_type, Empty, false);

        bool destroy = (consecutive_count >= 2);  // the player must find two or more consecutive blocks of the same color

        floodFillAndCountAndPossiblyDestroy(row, column, Empty, tapped_type, destroy);

        if (destroy)
            destroyed_blocks = consecutive_count;
        else
            board[row][column].state = Flashing;  // wrong move
    }

    incrementTimeout(destroyed_blocks);
    incrementScore  (destroyed_blocks);
}

bool GameplayScene::isBoardCompletelyEmpty()
{
    for (int r = 0; r < BOARD_SIZE; r++)
      for (int c = 0; c < BOARD_SIZE; c++)
        if (board[r][c].type != Empty)
            return false;
    return true;
}

bool GameplayScene::isBoardFull()
{
    for (int r = 0; r < BOARD_SIZE; r++)
      for (int c = 0; c < BOARD_SIZE; c++)
        if (board[r][c].type == Empty)
            return false;
    return true;
}

// The game ends when the countdown timer goes to 0
// or when there are no more consecutive or special blocks left on the board.
bool GameplayScene::isBoardFinished()
{
    for (int r = 0; r < BOARD_SIZE; r++)
      for (int c = 0; c < BOARD_SIZE; c++)
      {
        if (board[r][c].type == Stripe || board[r][c].type == Bomb)
            return false;

        if (r < BOARD_SIZE - 1)
          if (board[r][c].type == board[r + 1][c].type)
            return false;

        if (c < BOARD_SIZE - 1)
          if (board[r][c].type == board[r][c + 1].type)
            return false;
      }
    return true;
}

bool GameplayScene::isBoardSteady()
{
    for (int r = 0; r < BOARD_SIZE; r++)
      for (int c = 0; c < BOARD_SIZE; c++)
        if (board[r][c].state != Steady)
            return false;
    return true;
}

void GameplayScene::mouseReleased(ofMouseEventArgs & args)
{
    if (! isBoardSteady() || ! isBoardFull())
        return;

    for (int r = 0; r < BOARD_SIZE; r++)
      for (int c = 0; c < BOARD_SIZE; c++)
          if (board[r][c].insideAndTappable(args.x, args.y))
          {
            if (r == pressed_row && c == pressed_column)  // user must tap on a single block to make a valid move
            {
                tap(r, c);
            }

            invalidatePressed();

            return;
          }

    invalidatePressed();
}

void GameplayScene::mouseMoved(ofMouseEventArgs & args)  {}
void GameplayScene::mouseDragged(ofMouseEventArgs & args)  {}
void GameplayScene::mouseScrolled(ofMouseEventArgs & args)  {}
void GameplayScene::mouseEntered(ofMouseEventArgs & args)  {}
void GameplayScene::mouseExited(ofMouseEventArgs & args)  {}
