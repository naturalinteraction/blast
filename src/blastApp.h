
// © 2020 Natural Interaction


#pragma once

#include "ofMain.h"
#include "Scene.h"
#include "HomeScene.h"
#include "GameplayScene.h"
#include "GameoverScene.h"
#include "globals.h"


class blastApp : public ofBaseApp
{

    public:

        Scene *current_scene;

        void setup();
        void update();
        void draw();

        void gotMessage(ofMessage msg);

        void keyPressed(int key);
        void keyReleased(int key);
        void mouseMoved(int x, int y );
        void mouseDragged(int x, int y, int button);
        void mousePressed(int x, int y, int button);
        void mouseReleased(int x, int y, int button);
        void mouseEntered(int x, int y);
        void mouseExited(int x, int y);
        void windowResized(int w, int h);
        void dragEvent(ofDragInfo dragInfo);

};

