
// © 2020 Natural Interaction


#include "HomeScene.h"


// the home scene which shows the highest score and a button to start a new game
HomeScene::HomeScene()
{
    ofRegisterMouseEvents(this);

    start_new_game_button.setup("Start New Game", ofRectangle(2 * PADDING,
                                                              ofGetHeight() * 0.5,
                                                              320,                  // width
                                                              32),                  // height
                                "gameplay");  // message
}

HomeScene::~HomeScene()
{
    ofUnregisterMouseEvents(this);
}

void HomeScene::update()
{
    start_new_game_button.update();
}

void HomeScene::drawScore()
{
    ofSetColor(255);

    int integer_score = floor(highest_score);
    ofDrawBitmapString("Highest Score    " + to_string(integer_score),
                       PADDING * 3,
                       ofGetHeight() * 0.5 - PADDING * 4);

    ofDrawBitmapString("Blast!",
                       PADDING * 3,
                       ofGetHeight() * 0.5 - PADDING * 8);
}

void HomeScene::draw()
{
    ofFill();

    drawScore();

    start_new_game_button.draw();
}

void HomeScene::mousePressed(ofMouseEventArgs & args)
{
    // could be verified in mouseReleased()
    if (start_new_game_button.inside(args.x, args.y))
    {
        start_new_game_button.state = ButtonVanishing;
    }
}

void HomeScene::mouseMoved(ofMouseEventArgs & args)  {}
void HomeScene::mouseDragged(ofMouseEventArgs & args)  {}
void HomeScene::mouseReleased(ofMouseEventArgs & args)  {}
void HomeScene::mouseScrolled(ofMouseEventArgs & args)  {}
void HomeScene::mouseEntered(ofMouseEventArgs & args)  {}
void HomeScene::mouseExited(ofMouseEventArgs & args)  {}
