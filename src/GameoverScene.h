
// © 2020 Natural Interaction


#ifndef GameoverScene_h
#define GameoverScene_h

#include "ofMain.h"
#include "globals.h"
#include "Button.h"
#include "Scene.h"


class GameoverScene  :  public Scene
{

  public:

    GameoverScene();
    ~GameoverScene();

    void update();
    void draw();
    void drawScores();

    void mouseMoved(ofMouseEventArgs & args);
    void mouseDragged(ofMouseEventArgs & args);
    void mousePressed(ofMouseEventArgs & args);
    void mouseReleased(ofMouseEventArgs & args);
    void mouseScrolled(ofMouseEventArgs & args);
    void mouseEntered(ofMouseEventArgs & args);
    void mouseExited(ofMouseEventArgs & args);

    Button start_new_game_button, go_to_home_button;

};

#endif
