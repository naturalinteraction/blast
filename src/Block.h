
// © 2020 Natural Interaction


#ifndef Block_h
#define Block_h

#include "ofMain.h"
#include "globals.h"

enum BlockType  { Empty, Bomb, Stripe, Red, Blue, Green, Yellow, Violet, Orange };
enum BlockState { Steady, Appearing, Vanishing, Flashing };


class Block
{

  public:

    Block();
    ~Block();

    void setup(int row, int column);
    void update();
    void draw();

    float x();
    float y();

    bool insideAndTappable(float px, float py);

    int row;
    int column;

    BlockType type = Empty;
    BlockState state = Steady;

    float scale;

    bool flash;

};

#endif
