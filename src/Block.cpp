
// © 2020 Natural Interaction


#include "Block.h"

Block::Block()  {}

Block::~Block()  {}

void Block::setup(int row, int column)
{
    this->row = row;
    this->column = column;
    scale = 0.0;
}

bool Block::insideAndTappable(float px, float py)
{
    if (Empty == type || Steady != state)
      return false;

    ofRectangle r(x(), y(), BLOCK_SIZE, BLOCK_SIZE);
    return r.inside(px, py);
}

void Block::update()
{
    switch (state)
    {
      case Appearing:

          if (scale < 1.0)
            scale += BLOCK_SCALE_SPEED * ofGetLastFrameTime();

          if (scale >= 1.0)
          {
            scale = 1.0;
            state = Steady;
          }

      break;

      case Vanishing:

          if (scale > 0.0)
            scale -= BLOCK_SCALE_SPEED * ofGetLastFrameTime();

          if (scale <= 0.0)
          {
            scale = 0.0;
            type = Empty;
            state = Steady;
          }

      break;

      case Flashing:
          flash = true;
          state = Steady;
      break;

      case Steady:
      default:
      break;
    }
}

float Block::x()
{
    return 2 * PADDING + (PADDING + BLOCK_SIZE) * column;
}

float Block::y()
{
    return 2 * PADDING + (PADDING + BLOCK_SIZE) * row;
}

void Block::draw()
{
    ofColor c;

    switch (type)
    {
      case Empty:
        c = ofColor(0, 0);  // fully transparent
        break;

      case Red:
        c = ofColor(200, 0, 0);
        break;

      case Blue:
        c = ofColor(90, 90, 255);
        break;

      case Green:
        c = ofColor(0, 200, 0);
        break;

      case Yellow:
        c = ofColor(230, 230, 0);
        break;

      case Violet:
        c = ofColor(255, 0, 255);
        break;

      case Orange:
        c = ofColor(255, 100, 0);
        break;

      case Stripe:
      case Bomb:
      default:
        c = ofColor(0);  // black background
    }

    if (flash)
    {
      c = ofColor(255);
      flash = false;
    }

    ofSetColor(c);

    ofDrawRectRounded(x() + BLOCK_SIZE * (1.0 - scale) * 0.5,  // x
                      y() + BLOCK_SIZE * (1.0 - scale) * 0.5,  // y
                      BLOCK_SIZE * scale,   // width
                      BLOCK_SIZE * scale,   // height
                      PADDING);             // corners

    ofSetColor(255);  // white

    if (Stripe == type)
    {
      float stripe_width  = BLOCK_SIZE - 2 * PADDING;
      float stripe_height = BLOCK_SIZE * 0.3;

      ofDrawRectRounded(2 * PADDING + (PADDING + BLOCK_SIZE) * column + PADDING      + stripe_width  * (1.0 - scale) * 0.5,        // x
                      2 * PADDING + (PADDING + BLOCK_SIZE) * row + BLOCK_SIZE * 0.35 + stripe_height * (1.0 - scale) * 0.5,   // y
                      stripe_width  * scale,   // width
                      stripe_height * scale,   // height
                      PADDING);                // corners
    }

    if (Bomb == type)
    {
      ofDrawCircle(2 * PADDING + (PADDING + BLOCK_SIZE) * column + BLOCK_SIZE * 0.5,  // center x
                   2 * PADDING + (PADDING + BLOCK_SIZE) * row    + BLOCK_SIZE * 0.5,  // center y
                   BLOCK_SIZE * 0.3 * scale);    // radius
    }
}