
// © 2020 Natural Interaction


#include "Button.h"


Button::Button()
{
    scale = 0.0;
    tapped = false;
}

Button::~Button()  {}

void Button::setup(const string &text, const ofRectangle &rectangle, const string &command)
{
    this->text = text;
    this->rectangle = rectangle;
    this->command = command;
    scale = 0.0;
    state = ButtonAppearing;
    tapped = false;
}

void Button::update()
{
    if (state == ButtonAppearing)
    {
        if (scale < 1.0)
            scale += ofGetLastFrameTime() * BLOCK_SCALE_SPEED;
        if (scale >= 1.0)
        {
            scale = 1.0;
            state = ButtonSteady;
        }
    }

    if (state == ButtonVanishing)
    {
        if (scale > 0.0)
            scale -= ofGetLastFrameTime() * BLOCK_SCALE_SPEED;
        if (scale <= 0)
        {
            scale = 0.0;
            state = ButtonSteady;

            if (tapped)
                ofSendMessage(command);
        }
    }
}

void Button::draw()
{
    ofFill();

    if (flash)
    {
        ofSetColor(255);
        flash = false;
    }
    else
        ofSetColor(200, 0, 0);

    ofRectangle r;
    r.x = rectangle.x + 0.5 * rectangle.width  * (1.0 - scale);
    r.y = rectangle.y + 0.5 * rectangle.height * (1.0 - scale);
    r.width  = rectangle.width  * scale;
    r.height = rectangle.height * scale;

    ofDrawRectRounded(r,
                      PADDING);     // corner radius

    ofSetColor(255);

    if (scale > 0.9)
        ofDrawBitmapString(text, rectangle.x + 2 * PADDING, rectangle.y + 2.5 * PADDING);
}

bool Button::inside(float x, float y)
{
    bool yes = state == ButtonSteady && rectangle.inside(x, y);

    if (yes)
    {
        flash = true;
        tapped = true;
    }

    return yes;
}
